# Style Guide

## Naming

* Use verbs for function names except for getters.

```javascript
// bad
const buttonFactory = () = > {
  // ...
}

// good
const makeButton = () = > {
  // ...
}
```

* Prefix `is` or `has` to boolean variables.

```javascript
// bad
const active = false;

// good
const isActive = false;
```
